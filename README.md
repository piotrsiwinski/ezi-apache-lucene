#EZI Labs
Programming part from EZI

Authors: Piotr Siwiński, Mateusz Byczkowski

## Runing program
Clone repository:
``` 
git clone https://piotrsiwinski@bitbucket.org/piotrsiwinski/ezi-apache-lucene.git
```

To build jar (from root of project):
``` 
./mvnw clean package -DskipTests
```

To run from jar use:
```
java -jar target/luceneindex-1.0-SNAPSHOT-jar-with-dependencies.jar "path_to_text_documents"
```

Index will be stored in the same folder as "path_to_text_documents"

##Troubleshooting

In case of problems try:
```
./mvnw clean

```