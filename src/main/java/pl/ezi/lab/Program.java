package pl.ezi.lab;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Program {
    // TODO follows TODOs; there four places that should be filled with code
    // according to the instructions given in Lab6.pdf

    // directory where the index would be placed in
    // provided by the user as args[1]; set in main()
    public static String indexPath = "Shakespeare";

    // use: indexWriter to create an index of selected files
    public static void createIndex(String path) throws Exception {
        System.out.println("I'll will create index, feed it, and close it - still to be implemented");
        Directory directory = FSDirectory.open(Paths.get(path));

        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);

        File dirFile = new File(path);
        List<String> collect = Arrays.stream(Objects.requireNonNull(dirFile.listFiles()))
                .map(File::getAbsolutePath)
                .filter(e -> e.contains("0ws"))
                .collect(Collectors.toList());

        for (String s : collect) {
            indexWriter.addDocument(indexDoc(s));
        }
        indexWriter.commit();
        indexWriter.close();
    }

    // call this function in createIndex() to create Documents that would be subsequently added to the
    // index
    private static Document indexDoc(String docPath) throws IOException {
        try {
            Document doc = new Document();
            Field pathField = new StringField("path", docPath, Field.Store.YES);
            doc.add(pathField);
            Field contentField = new TextField("content", new FileReader(docPath));
            doc.add(contentField);
            return doc;
        } catch (IOException ioe) {
            throw ioe;
        }
    }

    // TODO create objects of class: IndexReader, IndexSearcher
    public static IndexSearcher getIndexSearcher() throws IOException {
        Path path = Paths.get(indexPath);
        Directory directory = FSDirectory.open(path);
        IndexReader indexReader = DirectoryReader.open(directory);
        return new IndexSearcher(indexReader);
    }

    // TODO create objects of class: Analyzer, IndexSearcher, QueryParser, Query and Hits
    // for Analyzer use standard analyzer
    // for QueryParser indicate the fields to be analyzed
    // for Query you should parse "queryString" which is given as a parameter of the function
    // for TopDocs you should search results (indexSearcher) for a given query and return 5 best
    // documents
    public static ScoreDoc[] processQuery(IndexSearcher indexSearcher, String queryString) throws IOException, ParseException {
        Analyzer analyzer = new StandardAnalyzer();
        QueryParser queryParser = new QueryParser("content", analyzer);

        Query query = queryParser.parse(queryString);
        TopDocs topDocs = indexSearcher.search(query, 5);
        ScoreDoc[] hits = topDocs.scoreDocs;
        return hits;
    }

    public static void main(String[] args) {

        //args = new String[2];
        // args[0] = TO SIMPLIFY DEBUGGING
        // args[1] = TO SIMPLIFY DEBUGGING

        if (args.length < 1) {
            System.out.println("java -jar luceneindex-1.0-SNAPSHOT-jar-with-dependencies.jar texts_path");
            System.out.println(
                    "need two args with paths to the collection of texts");
            System.exit(1);
        }
        try {
            //indexPath = args[1];
            String textsPath = indexPath;
            createIndex(textsPath);
            String query = " ";

            IndexSearcher indexSearcher = getIndexSearcher();

            // process queries until one writes "lab6"
            while (true) {
                Scanner sc = new Scanner(System.in);
                System.out.println("Please enter your query: (lab9 to quit)");
                query = sc.nextLine();

                if (query.equals("lab9")) {
                    break;
                } // to quit

                ScoreDoc hits[] = processQuery(indexSearcher, query);
                if (hits != null) {
                    System.out.println(hits.length + " result(s) found");

                    for (ScoreDoc hit : hits) {
                        try {
                            System.out.println("Id dokumentu: " + hit.doc + " : " + hit.score);
                            // read off and write its name or path (access from indexSearcher)
                            // and similarity score to the Console

                        } catch (Exception e) {
                            System.err.println("Unexpected exception");
                            System.err.println(e.toString());
                        }
                    }

                } else {
                    System.out.println("Processing the query still not implemented, heh?");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Even more unexpected exception");
            System.err.println(e.toString());
        }
    }
}
