package pl.ezi.lab;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class LuceneSearchApp {

    public LuceneSearchApp() {
    }

    public void index(List<RssFeedDocument> docs) {
        // implement the Lucene indexing here
        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        try {
            Directory directory = FSDirectory.open(Paths.get(""));
            IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);

            for (RssFeedDocument rssDoc : docs) {
                Document document = indexDoc(rssDoc);
                indexWriter.addDocument(document);
            }
            indexWriter.commit();
            indexWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Document indexDoc(RssFeedDocument rssDoc) {
        Document doc = new Document();
        Field titleField = new StringField("title", rssDoc.getTitle(), Field.Store.YES);
        Field dateField = new StringField("date", rssDoc.getPubDate().toString(), Field.Store.YES);
        Field descriptionField = new TextField("description", rssDoc.getDescription(), Field.Store.YES);
        doc.add(titleField);
        doc.add(dateField);
        doc.add(descriptionField);
        return doc;
    }

    public static IndexSearcher getIndexSearcher() throws IOException {
        Path path = Paths.get("");
        Directory directory = FSDirectory.open(path);
        IndexReader indexReader = DirectoryReader.open(directory);
        return new IndexSearcher(indexReader);
    }

    public List<String> search(List<String> inTitle, List<String> notInTitle, List<String> inDescription, List<String> notInDescription, String startDate, String endDate) {

        printQuery(inTitle, notInTitle, inDescription, notInDescription, startDate, endDate);
        // implement the Lucene search here
        String queryString = createStringQuery(inTitle, notInTitle, inDescription, notInDescription, startDate, endDate);

        List<String> results = new LinkedList<>();
        try {
            IndexSearcher indexSearcher = getIndexSearcher();
            Analyzer analyzer = new StandardAnalyzer();
            QueryParser queryParser = new QueryParser("content", analyzer);

            Query query = queryParser.parse(queryString);
            TopDocs topDocs = indexSearcher.search(query, 5);
            TopFieldDocs topFieldDocs = indexSearcher.search(query, 5, Sort.RELEVANCE);
            String field = topFieldDocs.fields[0].getField();
            ScoreDoc[] hits = topDocs.scoreDocs;
            Arrays.asList(hits).forEach(hit -> results.add(String.valueOf(hit.doc)));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return results;
    }

    private String createStringQuery(List<String> inTitle, List<String> notInTitle, List<String> inDescription, List<String> notInDescription, String startDate, String endDate) {
        StringBuilder sb = new StringBuilder();

        if (inTitle != null) {
            sb.append("(");
            sb.append("title:(");
            inTitle.forEach(str -> sb.append(str).append(" "));
            sb.append("))");
        } else {
            sb.append("title:").append("* ");
        }

        if (notInTitle != null) {
            sb.append(" NOT ");
            sb.append("(");
            sb.append("title:(");
            notInTitle.forEach(str -> sb.append(str).append(" "));
            sb.append("))");
        }

        if (inDescription != null) {
            sb.append(" AND ");
            sb.append("(");
            sb.append("description:(");
            inDescription.forEach(str -> sb.append(str).append(" "));
            sb.append("))");
        }

        if (notInDescription != null) {
            sb.append(" NOT ");
            sb.append("(");
            sb.append("description:(");
            notInDescription.forEach(str -> sb.append(str).append(" "));
            sb.append("))");
        }

        if (startDate != null && endDate != null) {
            sb.append(" AND ");
            sb.append("(");

            if (startDate.isEmpty()) {
                sb.append("date:[").append("*").append(" TO ").append(endDate).append("]");
            } else if (endDate.isEmpty()) {
                sb.append("date:[").append(startDate).append(" TO ").append("*").append("]");
            } else {
                sb.append("date:[").append(startDate).append(" TO ").append(endDate).append("]");
            }

            sb.append(")");
        }
        return sb.toString();
    }

    public void printQuery(List<String> inTitle, List<String> notInTitle, List<String> inDescription, List<String> notInDescription, String startDate, String endDate) {
        System.out.print("Search (");
        if (inTitle != null) {
            System.out.print("in title: " + inTitle);
            if (notInTitle != null || inDescription != null || notInDescription != null || startDate != null || endDate != null)
                System.out.print("; ");
        }
        if (notInTitle != null) {
            System.out.print("not in title: " + notInTitle);
            if (inDescription != null || notInDescription != null || startDate != null || endDate != null)
                System.out.print("; ");
        }
        if (inDescription != null) {
            System.out.print("in description: " + inDescription);
            if (notInDescription != null || startDate != null || endDate != null)
                System.out.print("; ");
        }
        if (notInDescription != null) {
            System.out.print("not in description: " + notInDescription);
            if (startDate != null || endDate != null)
                System.out.print("; ");
        }
        if (startDate != null) {
            System.out.print("startDate: " + startDate);
            if (endDate != null)
                System.out.print("; ");
        }
        if (endDate != null)
            System.out.print("endDate: " + endDate);
        System.out.println("):");
    }

    public void printResults(List<String> results) {
        if (results.size() > 0) {
            Collections.sort(results);
            for (int i = 0; i < results.size(); i++)
                System.out.println(" " + (i + 1) + ". " + results.get(i));
        } else
            System.out.println(" no results");
    }

    public static void main(String[] args) {
        if (true) {
            LuceneSearchApp engine = new LuceneSearchApp();

            RssFeedParser parser = new RssFeedParser();
            //parser.parse(args[0]);
            parser.parse("bbc_rss_feed.xml");
            List<RssFeedDocument> docs = parser.getDocuments();

            engine.index(docs);

            List<String> inTitle;
            List<String> notInTitle;
            List<String> inDescription;
            List<String> notInDescription;
            List<String> results;

            // 1) search documents with words "kim" and "korea" in the title
            inTitle = new LinkedList<String>();
            inTitle.add("kim");
            inTitle.add("korea");
            results = engine.search(inTitle, null, null, null, null, null);
            engine.printResults(results);

            // 2) search documents with word "kim" in the title and no word "korea" in the description
            inTitle = new LinkedList<String>();
            notInDescription = new LinkedList<String>();
            inTitle.add("kim");
            notInDescription.add("korea");
            results = engine.search(inTitle, null, null, notInDescription, null, null);
            engine.printResults(results);

            // 3) search documents with word "us" in the title, no word "dawn" in the title and word "" and "" in the description
            inTitle = new LinkedList<String>();
            inTitle.add("us");
            notInTitle = new LinkedList<String>();
            notInTitle.add("dawn");
            inDescription = new LinkedList<String>();
            inDescription.add("american");
            inDescription.add("confession");
            results = engine.search(inTitle, notInTitle, inDescription, null, null, null);
            engine.printResults(results);

            // 4) search documents whose publication date is 2011-12-18
            results = engine.search(null, null, null, null, "2011-12-18", "2011-12-18");
            engine.printResults(results);

            // 5) search documents with word "video" in the title whose publication date is 2000-01-01 or later
            inTitle = new LinkedList<String>();
            inTitle.add("video");
            results = engine.search(inTitle, null, null, null, "2000-01-01", null);
            engine.printResults(results);

            // 6) search documents with no word "canada" or "iraq" or "israel" in the description whose publication date is 2011-12-18 or earlier
            notInDescription = new LinkedList<String>();
            notInDescription.add("canada");
            notInDescription.add("iraq");
            notInDescription.add("israel");
            results = engine.search(null, null, null, notInDescription, null, "2011-12-18");
            engine.printResults(results);
        } else
            System.out.println("ERROR: the path of a RSS Feed file has to be passed as a command line argument.");
    }
}